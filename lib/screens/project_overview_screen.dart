import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../helper/i18n_helper.dart';
import '../model/abstract_timemanager_object.dart';
import '../model/client.dart';
import '../model/project.dart';
import '../provider/timemanager_provider.dart';
import '../widgets/app_drawer.dart';
import '../widgets/grid_items/project_grid_item.dart';
import '../widgets/search_grid.dart';

class ProjectOverviewScreen extends StatefulWidget {
  static const routeName = '/project/overview';

  @override
  _ProjectOverviewScreenState createState() => _ProjectOverviewScreenState();
}

class _ProjectOverviewScreenState extends State<ProjectOverviewScreen> {
  Client? _client;
  var _searchString = '';

  var init = true;

  @override
  Widget build(BuildContext context) {
    final tm = Provider.of<TimemanagerProvider>(context);
    if (init) {
      init = false;
      if (ModalRoute.of(context)!.settings.arguments != null) {
        _client = tm
            .get<Client>(ModalRoute.of(context)!.settings.arguments as String)!;
      }
    }
    List<Project> projects = _client == null
        ? tm.getAll<Project>().toList()
        : _client!.projects.toList();
    projects.sort((one, other) =>
        one.name.toLowerCase().compareTo(other.name.toLowerCase()));
    if (_searchString.isNotEmpty) {
      projects = projects
          .where(
              (c) => c.name.toLowerCase().contains(_searchString.toLowerCase()))
          .toList();
    }

    return Scaffold(
      drawer: _client == null ? AppDrawer() : null,
      appBar: AppBar(
        title: Text(
            _client == null ? 'general.projects'.tl(context) : _client!.name),
        actions: [
          if (_client != null)
            IconButton(icon: Icon(Icons.add), onPressed: updateProject),
        ],
      ),
      body: SearchGrid(
        childCount: projects.length,
        builder: (context, i) => ProjectGridItem(
          projects[i].uuid,
          () => updateProject(projects[i]),
          isRoot: _client == null,
        ),
        onSearchChanged: (s) {
          setState(() {
            _searchString = s;
          });
        },
        onRefresh: () => tm.sync(),
      ),
    );
  }

  void updateProject([Project? project]) async {
    String name = project == null ? '' : project.name;
    await showDialog<String>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(project == null
            ? 'project_screen.create_project'.tl(context)
            : 'project_screen.edit_project'.tl(context)),
        content: Row(
          children: [
            Expanded(
              child: TextFormField(
                initialValue: name,
                decoration: InputDecoration(
                  labelText: 'general.name'.tl(context),
                ),
                onChanged: (value) => name = value,
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {
              name = '';
              Navigator.of(context).pop();
            },
            child: Text(MaterialLocalizations.of(context).cancelButtonLabel),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(MaterialLocalizations.of(context).okButtonLabel),
          ),
        ],
      ),
    );
    if (name.isNotEmpty) {
      final tm = Provider.of<TimemanagerProvider>(context, listen: false);
      if (project == null) {
        tm.create(Project(tm, name: name, client_uuid: _client!.uuid));
      } else {
        tm.update(Project.copy(
          CopyBehaviour.updateCurrent,
          tm,
          project,
          client_uuid: project.client_uuid,
          name: name,
        ));
      }
      tm.sync();
    }
  }
}

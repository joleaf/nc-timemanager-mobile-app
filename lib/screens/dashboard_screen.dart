import 'package:flutter/material.dart';

import '../helper/i18n_helper.dart';
import '../widgets/app_drawer.dart';
import '../widgets/dashboard/chart.dart';
import '../widgets/dashboard/go_to_buttons.dart';
import '../widgets/dashboard/last_tasks.dart';
import '../widgets/dashboard/share_button.dart';

class DashboardScreen extends StatelessWidget {
  static const routeName = '/dashboard';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('general.dashboard'.tl(context))),
      drawer: AppDrawer(),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GoToButtons(),
            Divider(),
            ShareButton(),
            Divider(),
            LastTasks(6),
            Divider(),
            Chart(),
          ],
        ),
      ),
    );
  }
}

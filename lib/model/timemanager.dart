import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';

import 'abstract_timemanager_object.dart';
import 'client.dart';
import 'project.dart';
import 'task.dart';
import 'time.dart';

enum SyncResultStatus {
  success,
  noConnection,
  serverError,
  parseError,
  noCredentials,
}

abstract class Timemanager {
  static const _apiUrl = '/index.php/apps/timemanager/api/updateObjects';

  bool _certificateCheck(X509Certificate cert, String host, int port) => true;
  late IOClient _client;

  String? get user;

  String? get password;

  String? get url;

  var _lastCommit = '';
  final Set<AbstractTimemanagerObject> _objects = {};
  final Set<AbstractTimemanagerObject> _created = {};
  final Set<AbstractTimemanagerObject> _updated = {};
  final Set<AbstractTimemanagerObject> _deleted = {};

  Timemanager() {
    _client =
        IOClient(HttpClient()..badCertificateCallback = _certificateCheck);
  }

  T? get<T extends AbstractTimemanagerObject>(String uuid) =>
      _objects.firstWhere(
        (element) => element.uuid == uuid,
      ) as T;

  Set<T> getAll<T extends AbstractTimemanagerObject>() =>
      _objects.whereType<T>().toSet();

  AbstractTimemanagerObject? create(AbstractTimemanagerObject object) {
    if (!_objects.contains(object)) {
      _objects.add(object);
      _created.add(object);
      _deleted.remove(object);
      return object;
    }
    return null;
  }

  AbstractTimemanagerObject? update(AbstractTimemanagerObject object) {
    if (_objects.contains(object)) {
      _objects.remove(object);
      _objects.add(object);
      _updated.add(object);
      _deleted.remove(object);
      return object;
    }
    return null;
  }

  AbstractTimemanagerObject? delete(AbstractTimemanagerObject object) {
    if (_objects.contains(object)) {
      var dependedChildObjects = object.dependentChildObjects;
      _objects.remove(object);
      _objects.removeAll(dependedChildObjects);
      _deleted.add(object);
      _deleted.addAll(dependedChildObjects);
      return object;
    }
    return null;
  }

  Future<SyncResultStatus> sync({
    String? cachedJsonData,
    bool competeSync = false,
  }) async {
    print('start sync');
    if (url == null || user == null || password == null) {
      return SyncResultStatus.noCredentials;
    }
    var jsonData;
    if (cachedJsonData != null) {
      jsonData = cachedJsonData;
    } else {
      if (competeSync) {
        _lastCommit = '';
        _objects.clear();
      }
      final response =
          await _httpPost(Uri.parse(url! + _apiUrl), _toJsonSyncData());
      if (response == null) return SyncResultStatus.noConnection;
      if (response.statusCode != 200) return SyncResultStatus.serverError;
      jsonData = response.body;
    }
    if (await _parseData(jsonData)) {
      _created.clear();
      _updated.clear();
      _deleted.clear();
    } else {
      return SyncResultStatus.parseError;
    }
    return SyncResultStatus.success;
  }

  Future<http.Response>? _httpPost(Uri url, body) async {
    final basicAuth = 'Basic ' + base64Encode(utf8.encode('$user:$password'));
    final headers = {
      'authorization': basicAuth,
      'OCS-APIRequest': 'true',
      'Content-Type': 'application/json',
      'accept': 'application/json'
    };
    try {
      final httpResult = await _client.post(url, headers: headers, body: body);
      return httpResult;
    } on Exception catch (e) {
      return Future.value(null);
    }
  }

  String _toJsonSyncData() {
    final map = <String, dynamic>{
      'lastCommit': _lastCommit,
      'data': {
        'clients': {
          'created': _created.whereType<Client>().toList(),
          'updated': _updated.whereType<Client>().toList(),
          'deleted': _deleted.whereType<Client>().toList(),
        },
        'projects': {
          'created': _created.whereType<Project>().toList(),
          'updated': _updated.whereType<Project>().toList(),
          'deleted': _deleted.whereType<Project>().toList(),
        },
        'tasks': {
          'created': _created.whereType<Task>().toList(),
          'updated': _updated.whereType<Task>().toList(),
          'deleted': _deleted.whereType<Task>().toList(),
        },
        'times': {
          'created': _created.whereType<Time>().toList(),
          'updated': _updated.whereType<Time>().toList(),
          'deleted': _deleted.whereType<Time>().toList(),
        }
      }
    };
    return json.encode(
      map,
      toEncodable: (object) => (object as AbstractTimemanagerObject).toMap(),
    );
  }

  String toJson() {
    final map = <String, dynamic>{
      'lastCommit': _lastCommit,
      'data': {
        'clients': {
          'created': _objects.whereType<Client>().toList(),
          'updated': [],
          'deleted': [],
        },
        'projects': {
          'created': _objects.whereType<Project>().toList(),
          'updated': [],
          'deleted': [],
        },
        'tasks': {
          'created': _objects.whereType<Task>().toList(),
          'updated': [],
          'deleted': [],
        },
        'times': {
          'created': _objects.whereType<Time>().toList(),
          'updated': [],
          'deleted': [],
        }
      }
    };
    return json.encode(
      map,
      toEncodable: (object) => (object as AbstractTimemanagerObject).toMap(),
    );
  }

  Future<bool> _parseData(String jsonData) async {
    try {
      final dataMap = json.decode(jsonData);
      _lastCommit = dataMap['commit'];
      final results1 = (await Future.wait<Set<AbstractTimemanagerObject>>([
        _parseObjects<Client>(dataMap, 'created', createClient),
        _parseObjects<Client>(dataMap, 'updated', createClient),
        _parseObjects<Project>(dataMap, 'created', createProject),
        _parseObjects<Project>(dataMap, 'updated', createProject),
        _parseObjects<Task>(dataMap, 'created', createTask),
        _parseObjects<Task>(dataMap, 'updated', createTask),
        _parseObjects<Time>(dataMap, 'created', createTime),
        _parseObjects<Time>(dataMap, 'updated', createTime),
      ]))
          .expand((element) => element);
      _objects.removeAll(results1);
      _objects.addAll(results1);
      var results2 = (await Future.wait([
        _parseObjects<Task>(dataMap, 'deleted', createTask),
        _parseObjects<Time>(dataMap, 'deleted', createTime),
        _parseObjects<Project>(dataMap, 'deleted', createProject),
        _parseObjects<Client>(dataMap, 'deleted', createClient)
      ]))
          .expand((element) => element);
      _objects.removeAll(results2);
    } catch (e) {
      return false;
    }
    return true;
  }

  Client createClient(Timemanager tm, dynamic map) {
    return Client.fromMap(tm, map);
  }

  Project createProject(Timemanager tm, dynamic map) {
    return Project.fromMap(tm, map);
  }

  Task createTask(Timemanager tm, dynamic map) {
    return Task.fromMap(tm, map);
  }

  Time createTime(Timemanager tm, dynamic map) {
    return Time.fromMap(tm, map);
  }

  Future<Set<T>> _parseObjects<T extends AbstractTimemanagerObject>(
      Map<String, dynamic> dataMap,
      String path,
      Function(Timemanager, dynamic) mappingFunction) async {
    final data = dataMap['data'][T.toString().toLowerCase() + 's'][path]
        as List<dynamic>;
    final objects = <T>{};
    data.forEach((jsonMap) => objects.add(mappingFunction(this, jsonMap)));
    return objects;
  }

  @override
  String toString() {
    return _objects.toString();
  }
}

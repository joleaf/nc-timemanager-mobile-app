import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';

import '../model/time.dart';

class ShareTimes {
  static final String _SEP = ',';

  static Future<void> share(List<Time> times, BuildContext context) async {
    final path = await _localPath;
    final file = File('$path/times_${DateTime.now().toString()}.csv');
    var content = _header;
    times.sort((t1, t2) => t1.end.compareTo(t2.end));
    times.forEach((time) => content += _line(time, context));
    await file.writeAsString(content, flush: true);
    await Share.shareFiles([file.path], mimeTypes: ['text/csv']);
    file.delete();
    return;
  }

  static Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static String get _header =>
      'Client${_SEP}Project${_SEP}Task${_SEP}Time${_SEP}Date${_SEP}Duration in min\n';

  static String _line(Time time, BuildContext context) {
    return time.client.name +
        _SEP +
        time.project.name +
        _SEP +
        time.task.name +
        _SEP +
        time.note +
        _SEP +
        DateFormat.yMd(Localizations.localeOf(context).languageCode)
            .format(time.end) +
        _SEP +
        time.duration.inMinutes.toString() +
        '\n';
  }
}

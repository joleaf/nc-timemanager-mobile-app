class TimeConversionHelper {
  static DateTime? jsonToDateTime(String? json) {
    if (json == null || json == "undefined") {
      return null;
    }

    try {
      return DateTime.parse(json + "Z").toLocal();
    } catch (e) {
      //Ignore
    }

    try {
      return DateTime.parse(json).toLocal();
    } catch (e) {
      //Ignore
    }

    try {
      return DateTime.parse(json);
    } catch (e) {
      return null;
    }
  }

  static String? dateTimeToJson(DateTime? dateTime) {
    if (dateTime == null) {
      return null;
    }

    try {
      return dateTime.toUtc().toIso8601String().replaceAll("Z", "");
    } catch(e) {
      return dateTime.toIso8601String();
    }
  }
}

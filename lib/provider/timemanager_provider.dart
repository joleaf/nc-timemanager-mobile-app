import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import './nextcloud_auth_provider.dart';
import '../model/abstract_timemanager_object.dart';
import '../model/timemanager.dart';

class TimemanagerProvider extends Timemanager with ChangeNotifier {
  final _storage = FlutterSecureStorage();
  final NextcloudAuthProvider? _nextcloudAuthProvider;

  TimemanagerProvider(this._nextcloudAuthProvider) {
    if (_nextcloudAuthProvider != null) _tryLoadData();
  }

  bool get hasNcProvider => _nextcloudAuthProvider != null;

  @override
  Future<SyncResultStatus> sync({
    String? cachedJsonData,
    bool competeSync = false,
  }) async {
    final result = await super.sync(
      cachedJsonData: cachedJsonData,
      competeSync: competeSync,
    );
    notifyListeners();
    _storage.write(key: 'timemanager_data', value: toJson());
    return result;
  }

  @override
  AbstractTimemanagerObject? create(AbstractTimemanagerObject object) {
    final result = super.create(object);
    notifyListeners();
    return result;
  }

  @override
  AbstractTimemanagerObject? update(AbstractTimemanagerObject object) {
    final result = super.update(object);
    notifyListeners();
    return result;
  }

  @override
  AbstractTimemanagerObject? delete(AbstractTimemanagerObject object) {
    final result = super.delete(object);
    notifyListeners();
    return result;
  }

  Future<void> _tryLoadData() async {
    final value = await _storage.read(key: 'timemanager_data');
    // load local cache
    await sync(cachedJsonData: value);
    // load web
    await sync(competeSync: true);
  }

  @override
  String? get password =>
      _nextcloudAuthProvider == null ? null : _nextcloudAuthProvider!.password!;

  @override
  String? get url =>
      _nextcloudAuthProvider == null ? null : _nextcloudAuthProvider!.server!;

  @override
  String? get user =>
      _nextcloudAuthProvider == null ? null : _nextcloudAuthProvider!.user!;
}
